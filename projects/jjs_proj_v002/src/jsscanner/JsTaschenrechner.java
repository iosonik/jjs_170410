package jsscanner;

import java.util.Scanner;

public class JsTaschenrechner {

	public static void main (String[] args) {
		
		int zahl1 = 2;
		char operator = '+';
		int zahl2 = 17;
		
		int erg = executeOperation(operator, zahl1, zahl2);
		System.out.println(zahl1 + " " + operator + " " + zahl2 + " = " + erg);
		
		return;
		
	}
	
		static int readInt() {
			System.out.println("Geben Sie bitte die erste Zahl ein:");
			Scanner sc = new Scanner(System.in);
			return sc.nextInt();
			
		}
		
		
		static int executeOperation(char operator, int a, int b) {
			switch (operator) {
				case '+':
					return a + b;
				case '-':
					return a - b;
				case '/':
					return a / b;
				case '*' :
					return a * b;
				//default:
				//	break;
				
			}
			//Keine gute Lösung:
			//System.out.println("Fehler! Der Operator " + operator + " ist ungültig");
			return b; // um error Meldung oben zu korrigieren - return ausserhalb des switch
		}
}


