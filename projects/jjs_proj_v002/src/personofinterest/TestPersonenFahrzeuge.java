package personofinterest;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;

public class TestPersonenFahrzeuge {
    
    public static void main(String[] args) {
	
	Person p1 = new Person("Charlie", "Brown");
	Person p2 = new Person("Fred", "Astaire");
	Person p3 = new Person("Jimi", "Hendrix");
	
	Auto a1 = new Auto("RollsRoyce", "Silvershadow");
	Auto a2 = new Auto("VW", "Käfer");
	Auto a3 = new Auto("Opel", "Manta");
	Auto a4 = new Auto("Ford", "Mustang");
	
	// System.out.println("test" + " " + p1 + " " + p2 + " " + p3 + " " + a1 + " " + a2 + " " + a3 + " " + a4);
	
	Map<Person, Auto> mapPersonToAuto = new HashMap<Person, Auto>();
	mapPersonToAuto.put(p1,  a1);
	mapPersonToAuto.put(p2, a2);
	mapPersonToAuto.put(p3, a3);
	
	mapPersonToAuto.put(new Person("Harry", "Bellafonte"), a4);
	
	print(mapPersonToAuto); // see print method below
	
    }
	
	static void print(Map<Person, Auto> map) {
	    
	    Set< Map.Entry<Person, Auto> > entries = map.entrySet();
	    for( Map.Entry<Person, Auto> en : entries )
	    {
		System.out.println( en.getKey() + " hat einen " + en.getValue() );
	    }
	    
    }

}
