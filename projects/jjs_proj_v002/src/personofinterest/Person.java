package personofinterest;

public class Person {
	
	
	public String Vorname;
	public String Nachname;
//	public String Geburtsjahr; // short 16 bit max 32,767
//	public String Postleitzahl; // int 32 bit
	
	
	public Person (String Vorname, String Nachname) {
	// public Person (String Vorname, String Nachname, String Geburtsjahr, String Postleitzahl) {
		// no arg- or nullary -constructor of class oder Konstruktor aka Standard- parameterloser- Konstruktor
		
		this.Vorname = Vorname;
		this.Nachname = Nachname;
		// this.Geburtsjahr = "1931";
		// this.Postleitzahl = "00045";
	}

	
	@Override
	public String toString() {
	return Vorname + " " + Nachname;
	}
	
	@Override
	public int hashCode() {
	    return Vorname.hashCode() + Nachname.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
	    if ( obj.getClass() != Person.class) {
		return false;
	    }
	    
	    Person p2 = (Person)obj;
	    
	    return Vorname.equals(p2.Vorname) && Nachname.equals(p2.Nachname);
	}
	
	}

