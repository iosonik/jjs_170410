package personofinterest;

public class Individuals {

	public Individuals() {
		// no arg- or nullary -constructor of class oder Konstruktor aka Standard- parameterloser- Konstruktor
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		System.out.println( "******** Individual 1.:");
		Person indi1 = new Person();
		indi1.Vorname = "Bruce";
		indi1.Nachname = "Lee";
		indi1.Geburtsjahr = "1940";
		indi1.Postleitzahl = "94177";
		System.out.printf( " %s %s ist der Größte! Im Jahr %s wurde er im PLZ-Gebiet um %s herum geboren. Wo ist das?", indi1.Vorname, indi1.Nachname, indi1.Geburtsjahr, indi1.Postleitzahl);
		*/
		
		System.out.println( "******** Individual 2.:");
		Person indi2 = new Person("Frederico", "Benigni", "1952", "52100");
		System.out.printf( " %s %s ist groß! Im Jahr %s wurde er/sie im PLZ-Gebiet um %s herum geboren. Wo ist das?", indi2.Vorname, indi2.Nachname, indi2.Geburtsjahr, indi2.Postleitzahl);
		System.out.println();
		
		System.out.println( "******** Individual 3.:");
		Person indi3 = new Person("Johannes", "Sieknecht", "1971", "33333");
		System.out.printf( " %s %s ist groß! Im Jahr %s wurde er/sie im PLZ-Gebiet um %s herum geboren. Wo ist das?", indi3.Vorname, indi3.Nachname, indi3.Geburtsjahr, indi3.Postleitzahl);
		
		
	// public static String BuildIndividuals() {
		
		
	//	pers1.Vorname = Bruce;
	//	System.out.println("Vorname");
		
		// Nachname = Lee;
		// Geburtsjahr = 1940;
		// Postleitzahl = 94177;
	// }
	
}
}


