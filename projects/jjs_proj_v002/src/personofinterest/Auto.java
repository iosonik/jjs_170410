package personofinterest;

public class Auto {
    
    public String Firma;
    public String Modell;
    
    public Auto (String Firma, String Modell) {
	
	this.Firma = Firma;
	this.Modell = Modell;
	
    }
    
 
    @Override
	public String toString() {
	return Firma + " " + Modell;
	}
    

}
