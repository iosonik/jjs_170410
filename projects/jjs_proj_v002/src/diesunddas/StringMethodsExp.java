package diesunddas;

public class StringMethodsExp
{
 static String fnam = "ELvIs";
 static String lnam = "PrEslEy";
 static String FNAM = "ELVIS";
 static String LNAM = "PRESLEY";
 
 public static void main(String[] args)
 {
	 callMthCharAt();
	 callMthConcat();
	 callMthEqualsIgnoreCase();
	 callMthLength();
	 callMthReplace();
	 callMthSubstring();
	 callMthWatchIt();
	 callMthToLower();
	 callMthToString();
 }
 
 public static void callMthCharAt()  //chaAt
 { 
	 char mthResult = fnam.charAt(3);
	 System.out.println(mthResult);
	 System.out.println(fnam.charAt(3));
 }
 public static void callMthConcat()
 {
	 String mthResult = fnam.concat(" " + lnam);
	 mthResult += " Rodriguez";
	 System.out.println(mthResult);
	 System.out.println(mthResult + " " + "Lumbini");
 }
 public static void callMthEqualsIgnoreCase()
 {
	 boolean mthResult = fnam.equalsIgnoreCase(FNAM);
	 System.out.println(mthResult);
 }
 public static void callMthLength()
 {
	 int mthResult = fnam.length();
	 System.out.println(mthResult);
 }
 public static void callMthReplace()
 {
	 String mthResult = fnam.replace('v',  'P');
	 System.out.println(mthResult);
 }
 public static void callMthSubstring()
 {
	 String mthResult = fnam.substring(3,5);
	 System.out.println(mthResult);
 }
 public static void callMthWatchIt()
 {
	 String x = "tähst";
	 // System.out.println(x.length); // java.lang.Error - STRING - length is a method but an attribute is called ( without () )
	 System.out.println(x.length()); // works
	 
	 String[] y = new String[7];
	 // System.out.println(x.length()); // java.lang.Error - ARRAY - length is an attribute but a method is called ( with () )
	 System.out.println(y.length);
 }
 public static void callMthToLower() 
 {
	 String mthResult = FNAM.toLowerCase();
	 System.out.println(mthResult);
 }
 public static void callMthToString()
 {
	 String glue = fnam + LNAM + FNAM + lnam;
	 String mthResult = glue.toString();
	 System.out.println(mthResult);
 }
 
}
