package diesunddas;

public class OverloadOne
{
  static void test() { System.out.println("no args");}
  static void test(byte x) { System.out.println("byte...");}
  static void test(int x) { System.out.println("int...");}
  static void test(Integer x) { System.out.println("Integer...");}
  static void test(Number x) { System.out.println("Number...");} // ???
  static void test(Comparable<Integer> x) { System.out.println("Comparable<Integer>...");} // mit Comparable ambiguous
  static void test(float x) { System.out.println("float...");}
  static void test(int... x) { System.out.println("intVarg...");}
  static void test(long x) { System.out.println("long...");}
  
  
  public static void main (String[] args)
  {
  	long x = 2;
  	test(x);
  }
  
}




/*


- Die Reihenfolge der Methodendefinitionen ist egal
- Varargs methoden werdne nur dann in BEtracht gezogen, wenn nichts anderes passt (???)

Die folgende Liste der Suchpunkte wird bis zum Treffer abgearbeitet:

Bei einem primitiven Argument:

1. Methode mit der exakt gleichen Liste der Parametertypen (vgl. mit den Typen der Argumente)
2. Widening für den Argumenttyp:
		byte -> short -> int -> long -> float -> double
3. Bei primitiven Datentypen -> Autoboxing (Achtung, kein Widening nach bzw. bei Autoboxing!)
4.  nach dem Autoboxing aus Punkt 3!!!): Basistypparameter (!!!)
X. Es werden alle Punkte für die Varargs-Methoden durchgegangen


Bei Referenz-Argumenten:

1. Methode mit der exakt gleichen Liste der Parametertypen (vgl. mit den Typen der Argumente)
2. Basistypparameter (zB von Integer -> Number) 
3. Autounboxing (Achtung, kein Widening zum nächsten primitiven Typ nach bzw. bei Autounboxing!)
X. Es werden alle Punkte für die Varargs-Methoden durchgegangen

*/