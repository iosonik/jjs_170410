package Operatoren;

public class jsOperatoren {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// ^ XOR, & && AND, | || OR, ! NOT
		
		boolean b1 = true;
		boolean b2 = !b1;
		System.out.println("b2: " + b2); // wenn b2 auch true w�re, w�re das Ergbnis von println ebrenfalls true
		
		printAND(); // warum steht die Methode hier?? und warum funktioniert printAND unten nicht ohne??
		printANDSC();
		printOR(); 
		printORSC(); 
		printXOR();

	} // end of main
	
	static void printAND() {
		System.out.println("******** die AND-Logik ********");
		System.out.println("true & true = " + (true & true)); // true
		System.out.println("true & false = " + (true & false)); // false
		System.out.println("false & true = " + (false & true)); // false
		System.out.println("false & false = " + (false & false)); // false
		
	}
	
	static void printANDSC() {
		System.out.println("******** die ANDSC-Logik && konditional, Shortcircuit ********");
		System.out.println("true && true = " + (true && true)); // true
		System.out.println("true && false = " + (true && false)); // false
		System.out.println("false && true = " + (false && true)); // false
		System.out.println("false && false = " + (false && false)); // false
		
	}
		
		
	static void printOR() {
		System.out.println("******** die OR-Logik ********");
		System.out.println("true | true = " + (true | true)); // true
		System.out.println("true | false = " + (true | false)); // true
		System.out.println("false | true = " + (false | true)); // true
		System.out.println("false | false = " + (false | false)); // false
		
	}
			
	static void printORSC() {
		System.out.println("******** die ORSC-Logik || konditional, Shortcircuit ********");
		System.out.println("true || true = " + (true || true)); // true
		System.out.println("true || false = " + (true || false)); // true
		System.out.println("false || true = " + (false || true)); // true
			System.out.println("false || false = " + (false || false)); // false		
		
		
	}
		
		static void printXOR() {
			System.out.println("******** die XOR-Logik ********");
			System.out.println("true ^ true = " + (true ^ true)); // false
			System.out.println("true ^ false = " + (true ^ false)); // true
			System.out.println("false ^ true = " + (false ^ true)); // true
			System.out.println("false ^ false = " + (false ^ false)); // false
		
		
	}

}
