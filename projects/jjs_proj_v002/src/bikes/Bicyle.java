package bikes;

public class Bicyle
{
	private int cadence;
	private int gear;
	private int speed;
	
	public Bicyle(int startCadence, int startSpeed, int startGear) // wenn die Var. im Constructor den selben Namen wie die Inst-Var. hätten, wäre this. angebracht...
	{
		gear = startGear;
		cadence = startCadence;
		speed = startSpeed;
	}
	
	public int getCadence()
	{
		return cadence;
	}
	public void setCadence(int newValue)
	{
		cadence = newValue;
	}
	public int getGear()
	{
		return speed;
	}
	public void setGear(int newValue)
	{
		gear = newValue;
	}
	public int getSpeed()
	{
		return speed;
	}
	public void setSpeed(int newValue)
	{
		speed = newValue;
	}
}
