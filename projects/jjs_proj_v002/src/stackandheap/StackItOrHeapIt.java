package stackandheap;

// all method stacks are placed on the same stack - the application stack

class Student{
	int age; 					// instance variable
	String name; 				// instance variable
	
	public Student()
	{
		this.age = 0;
		name = "Anonymous";
	}
	public Student (int Age, String Name)
	{
		this.age = Age;
		setName(Name);
	}
	public void setName(String Name)
	{
		this.name = Name;
	}
}

// the main local variables, or variables stored on its STACK:
// the args reference (it is an array of strings)
// the student reference named s
// the integer (32bit) value, named noStudents 

public class StackItOrHeapIt {
	public static void main(String[] args) { // args reference is stored on stack (it is an array of Strings); 1. programm start "main()"
		Student s; 				// local variable - reference - stored on stack; 2. main() is calling Student s;
		s = new Student(23,"Jo"); // 3. the call stack has the max. size when it is called the setName method from the Student constructor
		int noStudents = 1;  	//local variable - the integer value (4 bytes) are stored on stack.
	}

}

/*Term: call stack = Memory that is used to save return address and local variables.
Term: stack frame = The storage on the call stack that is used by one method.
*/