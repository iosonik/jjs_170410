package stackandheap;

// Local variables live on the stack
// Instance variables and objects live on the heap

public class StackHeapSCJP {

}

class Collar {}

class Dog {
		Collar c;								// instance variable
		String name;							// instance variable
		
		public static void main(String[] args) { // 1. main() is placed on the stack
			
			Dog d;								// (local) reference variable d is created on the stack, but there's no Dog object yet
			d = new Dog();						// a new Dog object is created on the heap and is assigned to the d reference variable which lives on the stack
			d.go(d);							// a copy of the reference variable d is passed to the go() method which is on the stack.
		}
		void go(Dog dog) {						// the go() method is placed on the stack, with the dog parameter as a local variable on the stack, too.
			c = new Collar();					// a new Collar object is created on the heap, and assigned to Dog's instance variable which is also on the heap.
			dog.setName("Aiko");		
		}
		void setName(String dogName) {			// Method setName() is added to the stack, with the dogName parameter as its local variable on the stack, too.
			name = dogName;	
			// do more stuff
		}
}


/*
Notice that two different local variables refer to the same Dog object.
Notice that one local variable and one instance variable both refer to the same String Aiko.
After Line 28 completes, setName() completes and is removed from the stack. 
At this point the local variable dogName disappears too, although the String object it referred to is still on the heap.
*/
