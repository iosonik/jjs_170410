package utils;

public class ToStringDemo {
  
  public static void main(String[] args) {
      double valueAsNumber = 858.48;
      String valueAsString = Double.toString(valueAsNumber);
      
      System.out.println(valueAsNumber);
      
      int dotPositionInString = valueAsString.indexOf('.');
           
      System.out.println(dotPositionInString + " digits " +
          "before decimal point.");
      System.out.println( (valueAsString.length() - dotPositionInString - 1) +
          " digits after decimal point.");
      
      // Methods below
      
      System.out.println(TestCharAt());
      System.out.println(TestConcat());
  }
  
  public static char TestCharAt() 
  { // type casting via method type is possible (normal rules apply)!
  	String x = "murkxngrpZ";
  	return( x.charAt (9));
  }
  
  public static String TestConcat()
  {
  	String y = "lAmA";
  	System.out.println(y + " rAmA");
  	
  	String h = "hAmstEr";
  	h += " ! bAmstEr";
  	System.out.println(h );
  	
  	// String u = uwe; EXCEPTIONS
  	String  wiejetzt = null; // setzt die Variable wiejetzt auf null - NullPointerException, wenn wiejetzt über den Punkt-Operator eingebunden wird
  	System.out.println(wiejetzt.toUpperCase());
  	
  	String x = "MaXi";
  	return( x.concat(" TaXi"));
  	
  }
  
}

