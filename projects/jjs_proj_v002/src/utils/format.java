package utils;

import java.text.NumberFormat;

public class format
{
	public static void main(String[] args)
	{
		String helloFormat = String.format("Hallo %s \\ %s, %s!", "Tom", 1, true); // 1 und true werden ignoriert, wenn die letztten beiden Wildcards fehlen
		
		String helloForm = String.format("Hallo %s!!", "Jerry", 1, true);
		
		System.out.println(helloFormat);
		
		System.out.println(helloForm);

		System.out.printf("%s %s\t %s \n", "tee", 28, "hä");
		
		System.out.printf("%s %s %s %n", "text", 22, null);
		
		int[] anArray = new int[10];
		
		
		double[] v = { 1.0, 3.0, 5.0, 10.0 };
		double[] q = v;
			for ( int i=0 ; i<q.length ; i++ )
				q[i] = q[i]*q[i];
			for ( int i=0 ; i<q.length ; i++ )
				
				System.out.println( "Das Quadrat von " + v[i] + " ist " + q[i] );
	
			
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		
		String x1 = nf.format(12.2349345345);
		System.out.println(x1);
			
	}
}

