package schleifeneins;

class PrePostDemo {
  public static void main(String[] args){
      int i = 3;
      // prints 3
      System.out.println(i++);
      i++; // gets 4 and increments to 5
      // prints 5
      System.out.println(i);
      ++i;			   
      // prints 6
      System.out.println(i);
      // prints 7
      System.out.println(++i);
      // prints 7
      System.out.println(i++);
      // prints 8
      System.out.println(i);
  }
}