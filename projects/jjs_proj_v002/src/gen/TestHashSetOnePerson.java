package gen;

import java.util.HashSet;

class OnePerson {
    String Vorname, Nachname;

    public OnePerson(String Vorname, String Nachname) {
	this.Vorname = Vorname;
	this.Nachname = Nachname;
    }
    
    
    // end of OnePerson
    
public static class TestHashSetOnePerson {
    
    public static void main(String[] args) {
	
	HashSet<OnePerson> SetOnePerson = new HashSet<OnePerson>();
	
	OnePerson op1 = new OnePerson("Jürgen", "Prochnow");
	SetOnePerson.add(op1);
	SetOnePerson.add(op1);
	
	System.out.println(op1);
	System.out.println(SetOnePerson);
    }
}
    
}
