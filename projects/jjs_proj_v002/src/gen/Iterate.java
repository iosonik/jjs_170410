package gen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.math.*;

public class Iterate {
	
	public static void main(String[] args) {
	    
	    Collection<Integer> collInt = new ArrayList<Integer>();
	    
	    
	    
	    // bitte mit 10 Integer-Werten füllen
	    
	    for (int i = 0; i < 10; i++) {
		collInt.add(i); // autoboxing
	    }
	    
	    // alle Elemente mit while-Schleife untereinander ausgeben
	    
	    System.out.println("----- mit while: ");
	    Iterator<Integer> iterator = collInt.iterator();
	    while(iterator.hasNext()) {
		System.out.println(iterator.next());
	    }
	    
	    // alle Elemente mit for-Schleife untereinander ausgeben
	    
	    System.out.println("----- mit for: ");
	    for( Iterator<Integer> it = collInt.iterator(); it.hasNext(); ) {
		System.out.println(it.next());
	    }
	
	    
	    // alle Elemente mit foreach-Schleife untereinander ausgeben
	    
	    System.out.println("----- mit for-each: ");
	    for (Integer wert : collInt) {
		System.out.println(wert);
	    }
	}

}
