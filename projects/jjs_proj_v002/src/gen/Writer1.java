package gen;

import java.io.*;


public class Writer1 {
    
    public static void main(String[] args) {
	
	try {
	    File text = new File("text"); // create object text
	    System.out.println(text.isDirectory());
	    text.mkdir(); // make dir text
	    File dvdinf = new File( text, "dvdinfo.txt" ); // just an object
            PrintWriter pw = new PrintWriter(dvdinf); // make a PrintWriter object and make a file dvdinfo.txt to which dvdinf is assigned and assign pw to the printer
            System.out.println();
            
            pw.println("Donnie Darko/sci-fi/Gyllenhall, Jake");
            pw.println("Raiders of the Lost Ark/action/Ford, Harrison");
            pw.println("Lost in Translation/comedy/Murray, Bill");
            
            pw.flush();
            pw.close();
            
            FileReader fr = new FileReader(dvdinf);
            BufferedReader br = new BufferedReader(fr);
            
            String s;
            while ( ( s = br.readLine() ) != null)
		System.out.println(s);
		
	    br.close();
	} catch (IOException e) { }
    }

}


/*
Donnie Darko/sci-fi/Gyllenhall, Jake
Raiders of the Lost Ark/action/Ford, Harrison
Lost in Translation/comedy/Murray, Bill*/