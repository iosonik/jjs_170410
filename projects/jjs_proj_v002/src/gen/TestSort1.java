package gen;

import java.util.*;

class TestSort1 {
    
    public static void main(String[] args) {
	
	ArrayList<String> stuff = new ArrayList<String>(); // declare ArrayList of Strings
	
	stuff.add("Denver");
	stuff.add("Boulder");
	stuff.add("Vail");
	stuff.add("Aspen");
	stuff.add("Telluride");
	System.out.println("unsorted " + stuff);
	Collections.sort(stuff);			// sort alphabetically
	System.out.println("sorted " + stuff);
	
    }

}

