package gen;

class TvUk {}
class TvDe {}

class Steckdose {
	TvUk tv;
	
	void anschliessen(TvUk tv) {
		this.tv = tv;
	}
}

public class SteckdosenOhneGenerics {
	
	public static void main(String[] args) {
		
		// Es gibt dt. TV Geräte und Steckdosen
		// Es gibt engl. TV Geräte und Steckdosen
		
		TvUk tvUk = new TvUk();
	}

}
