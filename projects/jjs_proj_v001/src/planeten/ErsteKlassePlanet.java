package planeten;

public class ErsteKlassePlanet 
{
	
	public static void print(Raumschiff r) 	
	{
		System.out.println("Raumschiff: " + r.breite + " X " + r.hoehe);	
	}
	
	public static void main(String[] args) 	
	{		
		Raumschiff r = new Raumschiff();
		r.breite = 15;
		r.hoehe = 12;
		print(r);
		//r.ausgeben();
		
		Raumschiff r1 = new Raumschiff();
		r1.breite = 234;
		r1.hoehe = 34545;
		print(r1);
		System.out.println("Raumschiff: " + r1.breite + " x " + r1.hoehe);
	}
}
