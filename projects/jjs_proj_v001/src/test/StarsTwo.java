package test;

public class StarsTwo 
{

	public static void main(String[] args) 
	{
		int rows = 20;
		double maximumValue = Math.ceil(rows / 2.0);
		double shifted = maximumValue - 1;
		for (int i = 0; i < rows; i++) 
		{
		    int count = (int) (maximumValue - Math.abs(shifted - i));
		    if (i >= rows / 2 && rows % 2 == 0) // slight fix for even number of rows
		        count++;

		    for (int numStars = 0; numStars < count; numStars++) 
		    {
		        System.out.print("*");
		    }
		    System.out.println();
		}
		
		
	}
}