package test;

/** This was the code we created in lecture on 2017-02-13 */
public class Stars {

    public static void main(String[] args) {

        int maxStars = 5;

        printRightTriangle(maxStars, '*');
        printRightTriangle(4, 'x');
        //printRightTriangle(8);

        //printTriangle(maxStars);
        //printTriangle(3);

        //   *
        //  ***
        // *****
        System.out.println(makeCenteredTriangle(5));
    }

    public static String makeCenteredTriangle(int rows) {
        String result = "";
        for(int i = 0; i < rows; i++) {
            result += makeCharString(rows - i, ' ');
            result += makeCharString(2*i+1, '*') + '\n';
        }
        return result;
    }

    public static void printTriangle(int maxStars) {
        for(int i = 1; i <= maxStars; i++) {
            printStars(i);
        }
        for(int i = maxStars - 1; i > 0; i--) {
            printStars(i);
        }
    }

    public static void printRightTriangle(int rows, char c) {
//        for(int i = 0; i < rows; i++) {
//            printStars(i+1);
//        }
        System.out.println(makeRightTriangleString(rows, c));
    }

    public static String makeRightTriangleString(int rows, char c) {
        String result = "";
        for(int i = 0; i < rows; i++) {
            result += makeCharString(i+1, c) + "\n";
        }
        return result;
    }

    public static void printStars(int numStars) {
        System.out.println(makeStarString(numStars));
    }

    public static String makeStarString(int num) {
        return makeCharString(num, '*');
    }

    public static String makeCharString(int num, char c) {
        String result = "";
        for(int i = 0; i < num; i++) {
            result += c;
        }
        return result;
    }

}

